package com.oreillyauto.java.classes;

public enum Type {
    SALTY(1),
    MUDDY(2), 
    INSTANT(3);
    
    private final int typeCode;
    
    Type(int typeCode) {
        this.typeCode = typeCode;
    }
    
    public int getTypeInt(Type type) {
        return typeCode;
    }
    
    public String getTypeLiteral() {
        return this.name();
    }
}
