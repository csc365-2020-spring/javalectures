package com.oreillyauto.java.week1.day02;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.oreillyauto.java.week1.day02.pojos.Animal;
import com.oreillyauto.java.week1.day02.pojos.Cat;

public class W5D2TestHarness {

    public W5D2TestHarness() {
    	//testOperators();
        //testEquality();
        //testStrings();
    	//testControlFlow1();
    	//testControlFlow2();
        //testMaps();
    	testNumberObjects();
    	
        
    	//testMisc();
    	
    	// Find the sum of the digits for a given 
    	// integer in one line of code
    	//System.out.println(testStream(1234));
    	
    	//testHash();
    	//testLong();
    	
    	//StringBuilder

    	//Replace characters, regex

    	//BigDecimal division, precision, scale, rounding

    	/*- Creating Arrays - 
    	new int[]{1,2,3};
    	new Integer[]{1,2,3};
    	new String[]{"1","2","3"};*/
        
        // Talk about constants / uppercase
        // public static final String FOO_BAR = "Foo Bar";
        
    }

    private void testNumberObjects() {		
		// Division and Rounding with precision via MathContext
//		BigDecimal solution;
//		MathContext mc = new MathContext(2); // 
//		BigDecimal bd = new BigDecimal("1").divide(new BigDecimal("8"));
//		solution = bd.round(mc);
//		System.out.println("solution = " + solution);
		
		// Division and Rounding with precision w/o MathContext
//		BigDecimal solution;
//		BigDecimal bd = new BigDecimal("1").divide(new BigDecimal("8"), 2, BigDecimal.ROUND_HALF_UP);
//		// use .divide(divisor, scale, roundingMode);
//		System.out.println("solution = " + bd);

    	// Non-Terminating Division .33333
//		BigDecimal bd2 = new BigDecimal("1").divide(new BigDecimal("3"));
//		bd2 = bd2.round(new MathContext(2));
//		System.out.println(bd2); // output?
		
		// non-terminating solution - scale (2) represents values after the decimal
//		BigDecimal bd2 = new BigDecimal("1").divide(new BigDecimal("3"), 2, BigDecimal.ROUND_HALF_UP);
//		System.out.println(bd2); // output?
		
		/**  Declaration Examples */
		// Float (Object) - requires "f" or "F"
		Float f = 1f/3f;
		float f2 = 1/3; // no 'f'
		Float f3 = 1F/3F;
		
		// Double (Object) - requires "d" or "D"
		Double d = 1d/3d;
		double d2 = 1/3; // no 'd'
		Double d3 = 1D/3D;
		
		// Long (Object) - requires "L" or "l" 
		Long myLong = 3L;
		
		// FYI: BigInteger is Immutable
		BigInteger bi = new BigInteger("55555555555555"); 
	}

	private void testLong() {
    	// Declare long
    	long l1 = 1l;
    	
    	// Declare Long (upper or lower case L)
    	Long l2 = 1l;
    	Long l3 = 1L;
    	
    	// Pass long "on-the-fly"
		printLong(1L);
	}
    
    private void printLong(Long l) {
    	System.out.println("Long = " + l);
    }

	private void testHash() {
		Animal animal1 = new Animal("bird");
		Animal animal2 = new Animal("bird");
		System.out.println("animal1.equals(animal2) : " + (animal1.equals(animal2)));
		System.out.println("animal1 hash: " + animal1.hashCode());
		System.out.println("animal2 hash: " + animal1.hashCode());
	}

	private int testStream(int myInt) {
		//char y = '7';
		//System.out.println("y=" + (y - '0'));
//		List<String> list = Arrays.asList("abc1", "abc2", "abc3");
//		int counter = 0;
//		Stream<String> stream = list.stream().filter(element -> {
//		    return element.contains("2");
//		});
		
    	return String.valueOf(myInt).chars().map(x -> x - '0').sum();
	}

	/**
     * TEST MISC
     * Use this method to test intern questions
     */
	private void testMisc() {
		// Point (x,y) - can be used for 2D positioning
//		Point point = new Point(4,5);		
//		System.out.printf("x= %f and y=%f", point.getX(), point.getY());
		
	}

	/**
	 * TEST CONTROL FLOW 1
	 */
	private void testControlFlow1() {
		int sum = 5;
		
		/** if */
//		if (sum == 5) {
//		    System.out.println("sum=5");
//		}

		/** if else */
//		if (sum == 5) {
//		    System.out.println("sum equals 5");
//		} else {
//		    System.out.println("sum does not equal 5");
//		}

		/** switch */
//		sum = 8;
//
//		switch (sum) {
//		default:
//			System.out.println("sum does not equal 1, 2, or 3");
//			break;
//		case (1):
//			System.out.println("sum equals 1");
//			break;
//		case (2):
//			System.out.println("sum equals 2");
//			break;
//		case (3):
//			System.out.println("sum equals 3");
//			break;
//		case (8):
//			System.out.println("sum equals 8");
//			break;
//		}
		
		
		/** What's the output? */
//		int x = 2;
//
//		switch (x) {
//		default:
//			System.out.println("x does not equal 2");
//		case (1):
//			System.out.println("x equals 1");
//		case (3):
//			System.out.println("x equals 3");
//		}
		
		/** while */
//		int count = 1;
//        
//		while (count < 3) {
//		    count += 1;
//		}
//		        
//		System.out.println("Done. count=" + count);

		
		/** do while */
//		int count = 1;
//        
//		do {
//		    count += 1;
//		} while (count < 3);
//		        
//		System.out.println("Done. count=" + count);

		
		/** for loops */
		Integer[] valuez = {1,2,3}; // one way
		List<Integer> values = Arrays.asList(new Integer[]{1,2,3}); // another way
		List<String> listOfStrings = Arrays.asList(new String[]{"4","5","6"});
		
		/** Notice that the length of an array is a property, like in JavaScript */
//		System.out.println(valuez.length);
		
		/** Notice that Arrays use .length (property) */ 
		/** while Lists use .size() (method)          */
//		System.out.print("values: ");
//		for (int i = 0; i < values.size(); i++) {
//		    System.out.print(values.get(i)); // notice print vs println
//		}

//		System.out.print("listOfStrings: ");
//		for (String temp : listOfStrings) {
//		    System.out.print(temp);
//		}

		// Conditions in for loop are optional (none)
//		for (;;) {
//			System.out.println("hello world!");
//			break;
//		}
		
		// Conditions in for loop are optional (one)
//		for (int i=0; /*optional*/ ; /*optional*/) {
//		    System.out.print("processing (i+=1)="+(i+=1));
//		    i+=1;
//		    
//		    if (i==2) {  // condition to break loop
//		        break;    
//		    }
//		}

		/** ArrayList forEach() method performs the argument statement/action 
		 *  for each element of the list until all elements have been processed 
		 *  or the action throws an exception. (Java 8+)
		 *  
		 *  By default, actions are performed on elements taken in the order 
		 *  of iteration.
		 */
//		System.out.print("animalList: ");
//		List<String> animalList = Arrays.asList(new String[]{"Cat", "Dog"});
//		animalList.forEach((name) -> System.out.print(name.toLowerCase() + " "));
//		System.out.println();

	}

	/**
	 * TEST CONTROL FLOW 2
	 */
	private void testControlFlow2() {
		/** Break */
//        int sum = 0;
//        
//        for (int i = 1; i < 3; i++) {
//            for (int j = 1; j < 3; j++) {
//                if (j == 2) {
//                    break;            // break on the innermost loop 
//                } else {              // outer loops continue to process.
//                    sum += (i + j);    
//                }
//            }   
//        }
//        
//       System.out.println("sum="+sum);


		/** Continue */
//        int sum = 0;
//        
//        for (int i = 0; i < 5; i ++) {
//            if (i == 3) {
//                continue;
//            } else {
//                sum += i;
//            }
//        }
//        
//        System.out.println("sum="+sum);

       
       /** Goto */	
//		outer:
//		for (int i = 0; i < 999999999; i++) { 
//			for (int j = 5; j > -1; j--) {
//				if (j == 0) {
//					break outer; // "GOTO" outer loop when j = 0
//				}
//				System.out.println("value of j = " + j);
//			}
//		} // end of outer loop
//		
//		System.out.println("Blastoff!");
		
		
	} // end testControlFlow2
	
	
	/**
	 * TEST OPERATORS
	 */
	private void testOperators() {
		int sum = 1;
		
		/** Prefix and Postfix Notation */ 
//		System.out.println("sum++ => " + (sum++));
//		sum = 1;
//		System.out.println("++sum => " + (++sum));

		/** Standard Mathematics */
//		sum = 1;
//		System.out.println("sum*5 => " + (sum * 5));

//		sum = 1;
//		System.out.println("sum/1 => " + (sum / 1));

//		sum = 1;
//		System.out.println("sum%1 => " + (sum % 1));

		/** Booleans */
//		boolean fast = true;
//		System.out.println("!fast => " + (!fast));

//		Integer foo1 = 1;
//		Integer foo2 = 2;
//		System.out.println("foo1(1) == foo2(2) => " + (foo1 == foo2));
//		System.out.println("foo1(1) < foo2(2) => " + (foo1 < foo2));
//		System.out.println("foo1(1) > foo2(2) => " + (foo1 > foo2));
//		System.out.println("foo1(1) instanceof Integer => " + (foo1 instanceof Integer));
//		
//		System.out.println("foo1(1) != foo2(2) => " + (foo1 != foo2));
		/** Notice we can use instanceof to find if one object
		 *  is an instance of another.
		 */
		
		/** Booleans and Ternarys */
//		boolean salty = true;
//		boolean sweet = false;
//
//		System.out.println("isSaltyOrSweet(||) => " + ((salty || sweet) ? true : false));
//		System.out.println("isSaltyAndSweet(&&) => " + ((salty && sweet) ? true : false));
		
		
		/** instanceof */
		// Can we determine if int is an instanceof Integer?
//		int foo3 = 3;
//		System.out.println("foo3(3) instanceof Integer => " + (foo3 instanceof Integer));
		// Incompatible conditional operand types int and Integer
		
		// Custom Objects
//		Cat cat = new Cat();
//		System.out.println("(cat instanceof Cat) => " + (cat instanceof Cat)); // t/f?
//		System.out.println("(cat instanceof Animal) => " + (cat instanceof Animal)); // t/f?
				
		/** Conditional Operators */
		/** Notice with Strings, we do NOT use == but instead use .equals() */
		/** We can also use .equalsIgnoreCase()                             */
		String word1 = "word1";
		String word2 = "word2";
		String word3 = null;
		
//		System.out.println("1&&2&&3 => " + (word1.equals("word3") && 
//				word2.equals("word2") && word3.equals("word1"))); // t/f?
		
//		System.out.println("word3.equals(\"word1\")" + word3.equals("word1")); // t/f?
		
//		System.out.println("word1.equals(word3) => " + word1.equals(word3)); // t/f/npe?
	}

	/**
	 * TEST MAPS
	 */
	private void testMaps() {
        /** Build an empty map                                   */
		/** Notice that we are using the interface "Map" to      */
		/** declare the map and we are using the implementation  */
		/** HashMap to initialize the map. This is best practice.*/		
        Map<String, String> userMap = new HashMap<String, String>();

        // Populate the map
        userMap.put("firstName", "Bruce");
        userMap.put("lastName", "Wayne");

        String firstName = userMap.get("firstName");
//        System.out.println("firstName: " + firstName);
        
        // Print the size of the map
//        System.out.println("Map Size: " + userMap.size());
        
        // Get the Iterator from the Map (Harder way to iterate a Map)
        Iterator<Entry<String, String>> mapIterator = userMap.entrySet().iterator();

        // Loop through the map to obtain the key and value
//        while (mapIterator.hasNext()) {
//            Map.Entry<String, String> pair = (Map.Entry<String, String>) mapIterator.next();
//            System.out.println(pair.getKey() + " = " + pair.getValue());
//        }
        
        // Another way to iterate the userMap (Easier). Throws ConcurrentModificationException
        // when the map contains one or more entries.
//        for (Map.Entry<String, String> entry : userMap.entrySet()) {
//            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
//            userMap.remove(entry.getKey()); 
//        }
        
        /** 
         * Concurrent Modification Exception
         * It is not generally permissible for one thread to modify a Collection while 
         * another thread is iterating over it. In general, the results of the iteration 
         * are undefined under these circumstances. Some Iterator implementations 
         * may choose to throw this exception if this behavior is detected. Iterators that 
         * do this are known as fail-fast iterators, as they fail quickly and cleanly, rather 
         * that risking arbitrary, non-deterministic behavior at an undetermined time in 
         * the future. 
         */

        // Iterate, print, and remove items in the Map (without an exception)
//        while (mapIterator.hasNext()) {
//        	Map.Entry<String, String> pair = (Map.Entry<String, String>) mapIterator.next();
//        	System.out.println(pair.getKey() + " = " + pair.getValue());
//        	mapIterator.remove();
//        }
//        
//        // Print the size of the map again (should be zero)
//        System.out.println("Map Size: " + userMap.size());
    }

	/**
	 * TEST STRINGS
	 */
    private void testStrings() {
    	
    	/** String Value Example */
//    	Integer i = new Integer(5);
//    	String iStr = String.valueOf(i);
//    	System.out.println("String.valueOf(i) : " + iStr + " iStr instanceof String: " +  (iStr instanceof String));
//    	System.out.println("i.toString(): " + i.toString());
//    
//    	Integer n = 5; // initialize with a "primitive" number. Integer or no?
//    	System.out.println("n(5) instanceof Integer : " +  (n instanceof Integer));
    	
    	
    	/**
    	 *  Concatenation Example using + 
    	 */
//        String a = "alpha";
//        String b = "beta";
//        String c = a + b;
//        System.out.println("a+b: " + c);
        
        /** 
         * Concatenation using StringBuilder 
         * This example performs well since StringBuilder is not immutable; meaning 
         * the value can be updated over each iteration.
         * a = a + a + b + b;   // Not performant!!
         */
//        StringBuilder sbr = new StringBuilder();
//        System.out.println("StringBuilder: " + sbr.append("a").append("a").append("b").append("b"));
//        System.out.println("sbr: " + sbr);
        
        /** 
         * Concatenation using StringBuffer 
         */
//        StringBuffer sb = new StringBuffer();
//        System.out.println("StringBuffer: " + sb.append("a").append("a").append("b").append("b"));
        
    	
/**    	
  		Difference Between StringBuilder and StringBuffer
    		Operations are almost identical. 
    		The class [StringBuilder] provides an API compatible with StringBuffer, 
    	    but with no guarantee of synchronization. 
    		The class [StringBuilder] is for use as a drop-in replacement for 
    	    StringBuffer in places where StringBuffer was used by a single thread 
    	    (as is generally the case). 
    		Where possible, use StringBuilder over StringBuffer because it is faster 
    	    under most implementations.
*/

    	
    	
        /** 
         * Insert Example 
         */
//		StringBuilder sb = new StringBuilder();
//		sb.append("sb");
//		sb.insert(1, "helo");
//		System.out.println("sb: " + sb);
       
    	
		/** CharAt */
//		String name = "Batman";
//    	
//		for (int index=0; index < name.length(); index++ ) {
//			System.out.println("c=" + name.charAt(index));
//		}
		
		/** char[] charArray */
//    	String name = "Robin";
//		char[] charArray = name.toCharArray();
//    	
//		for (char c : charArray) {
//			System.out.println("c=" + c);
//		}
    	
    	
    	/** String[] via split() */
//    	String name = "Alfred";
//		String[] charArray = name.split("");
//		
//    	for (String string : charArray) {
//    		System.out.println("s=" + string);
//		}
    	
    	/** Replace - Example 1 */
//    	String foobar = "foobar";
//    	String result = foobar.replace("oo", "i");
//    	System.out.println("result: " + result);
    	
    	/** Replace - Example 2 */
//    	String barfoo = "barfoo";
//    	String result2 = barfoo.replace('o', 'e');
//    	System.out.println("result2: " + result2);
    	
    	/** Replace - Example 3 */
//    	String hello = "hello";
//    	System.out.println("hello.replace(\"o\", \"a\"): " + hello.replace("o", "a"));
//    	System.out.println("hello: " + hello);
    	
    	/** Replace - Example 4 */
//    	String mySocial = "555-55-5555";
//    	mySocial = mySocial.replaceAll("[0-9]", "X");
//    	System.out.println("mySocial: " + mySocial);
    }

    /**
     * TEST EQUALITY
     */
    private void testEquality() {
    	/** We used .equals() with Strings. With numbers, though,     */
    	/** we will use "=="                                          */
//        int foo = 3;
//        int bar = 4;
//        
//        if (foo == bar) {
//            System.out.println("equal");
//        }
//
//        if (foo != bar) {
//            System.out.println("not equal");
//        }
//
//        if (foo >= bar) {
//            System.out.println("foo equal or > bar");
//        }
//        
//        if (foo <= bar) {
//            System.out.println("foo < or equal bar");
//        }        
               

        /** Test Animal Equality                            */
    	/** When comparing objects, use .equals()           */
//        Animal cat1 = new Animal("Cat");
//        Animal cat2 = new Animal("Cat");
//        
//        if (cat1.equals(cat2)) {
//            System.out.println(".equals: cat1 equals cat2");
//        } else {
//            System.out.println(".equals: cat1 does not equal cat2");
//        }
//        
//        // Bad Implementation - do NOT use == when comparing two objects
//        if (cat1 == cat2) {
//            System.out.println("==: cat1 equals cat2");
//        } else {
//            System.out.println("==: cat1 does not equal cat2");
//        }
        
        // stop 
        // stop
        
        
        // Uncomment hashCode and equals in Animal.java and Cat.java and try again
        
    	
    	/** Create two Cat objects that are different and compare */    	
//		Cat cat1 = new Cat("Tabby", "Green");
//		Cat cat2 = new Cat("Tabby", "RebeccaPurple");
//		  
//		if (cat1.equals(cat2)) {
//			System.out.println("cat1 equals cat2");
//		} else {
//			System.out.println("cat1 does not equal cat2");
//		}
		
		/** Comment the equals and hash in Cat.java and run again   */
		/** The comparison will be performed using the super class  */
		
		
        /** Test Object Classes                                     */
    	/** When we subclass an Object, does the class take on the  */
    	/** super class's class name?                               */
//		Animal animal1 = new Animal("Cat");
//		Animal animal2 = new Cat("Cat");
//		System.out.println("animal1(animal).getClass(): " + animal1.getClass().getSimpleName());
//		System.out.println("animal2(cat).getClass()   : " + animal2.getClass().getSimpleName()); // cat or animal?
    }

    /**
     * COUNT
     * @param array
     * @return
     */
    public int count(Integer[] array) {
        int sum = 0;
       
        for (Integer i : array) {
            sum = sum + i;
        }
        
        return sum;
    }

    /**
     * COUNT
     * @param start
     * @param integers
     * @return
     */
    public int count(Integer start, Integer...integers) {
        int sum = start;   

        for (Integer i : integers) {
            sum = sum + i;
        }
        
        return sum;
    }

    /**
     * MAIN
     * @param args
     */
    public static void main(String[] args) {   	
    	new W5D2TestHarness();
    }

}
