package com.oreillyauto.java.week1.day02.pojos;

public class Animal {
    
    public final static String COLOR_BLACK = "BLACK";
    public final static String COLOR_PURPLE = "PURPLE";
    public final static String COLOR_RED = "RED";
    public final static String COLOR_WHITE = "WHITE";
    
    public final static String TYPE_FELINE = "FELINE";
    public final static String TYPE_CANINE = "CANINE";
    
    public String type = "";
    public String color = "";
    
    public Animal() {}

    public String sound() {
    	return "meh";
    }
    
    protected String habitat() {
    	return "world";
    }
    
    public Animal(String type) {
        this.type = type;
    }

    public Animal(String type, String color) {
        this.type = type;
        this.color = color;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

/*    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)   // this is equal to current Animal
            return true;
        
        if (that == null)   // object that is passed in
            return false;
        
        if (getClass() != that.getClass())  // do the classes even match?
            return false;
        
        Animal other = (Animal) that;  // type casting
        
        if (type == null) {
            if (other.type != null)   // are both types null?
                return false;
        } else if (!type.equals(other.type)) { // are both types equal?
            return false;
        }
        
        // optional: add comparison of other member variables here
        
        return true; // equals! yay!
    }*/

    @Override
    public String toString() {
        return "Animal [type=" + type + ", color=" + color + "]";
    }
 
}
