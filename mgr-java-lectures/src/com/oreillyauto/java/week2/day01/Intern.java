package com.oreillyauto.java.week2.day01;

public class Intern extends ActiveEmployee {
	
	// Compilation Exception: Cannot override the final method from ActiveEmployee
/*    public void getStatus() { 
        
    }*/
}
