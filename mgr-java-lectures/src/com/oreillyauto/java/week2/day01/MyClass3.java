package com.oreillyauto.java.week2.day01;

public class MyClass3 {

	public int incrementing = 0;
	public int nonStaticVariable = 0;

	public MyClass3() {
//		testStaticInnerClass();
//		testStaticMethod();
		testTimsClass();
	}

    private void testTimsClass() {
    	TimsClass timsClass = new TimsClass();
    	System.out.println(timsClass.foobar);
	}

	private void testStaticMethod() {
        System.out.println(MyClass3.getInfoFromStaticMethod());        
    }
    
	private void testStaticInnerClass() {
        System.out.println(MyClass3.MyInnerClass.getInnerMethod());
    }

	private static String getInfoFromStaticMethod() {
		return "foo";
	}

	private static class MyInnerClass {
		private static String getInnerMethod() {
			return "foo2";
		}
	}

	private class MyInnerClass3 {
		private String getInnerMethod2() {
			return "foo2";
		}
	}
	
	public static void main(String[] args) {
		new MyClass3();
	}
	
	public class TimsClass {
		private String foobar = "foobar";
		
	}
	
}
