package com.oreillyauto.java.week2.day03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Week6Day3 {

    public Week6Day3() {
    	
    	/** Build Management Tools */
    	// Ant, Maven, Gradle
    	
    	/** Unit Testing */
    	// JUnit, Spock
    	
    	/** Streams */
//    	streamExample1();
//        startRandomStream();
//        startStreamWithIterate();
//        stringsAndFilter();
//        numbersToArrayList();
    	
    	/** Reference By Value Examples */
//        attemptSwap();
//        referenceObjects();
    	provePassObjectByValue();
    }

    /**
     *  David Flanagan says, "Java manipulates objects 'by reference,' but it 
     *  passes object references to methods 'by value.'"
     * 
     *  We saw that we can pass an object (minivan) to a method, change it, and
     *  then print it and it is modified. It "seems" as though we are passing by
     *  reference, but we are not. 
     * 
     *  If we are passing by reference, we should be able to run a swap operation
     *  over an object and the original should be updated. Let's test it...
     */
	private void provePassObjectByValue() {
		Vehicle civic = new Vehicle("car", "Honda Civic");
		Vehicle minivan = new Vehicle("minivan", "Dodge Caravan RT");
        System.out.println("civic before swapVehicle: \t\t" + civic);
        System.out.println("minivan before swapVehicle: \t\t" + minivan);
        swapVehicle(minivan, civic);
        System.out.println("civic after swapVehicle: \t\t" + civic);
        System.out.println("minivan after swapVehicle: \t\t" + minivan);
        // If they are swapped, then Java passes by reference!
	}

	private void swapVehicle(Vehicle minivan, Vehicle civic) {
        Vehicle cupHolder = minivan;
        minivan = civic;
        civic = cupHolder;
        System.out.println("civic inside swapVehicle: \t\t" + civic);
        System.out.println("minivan inside swapVehicle: \t\t" + minivan);
        
	}

	private void streamExample1() {
		List<Integer> intList = Arrays.asList(new Integer[] {1,2,3});
		
		// Returns a sequential Stream with this collection as its source. 
		Stream<Integer> stream = intList.stream();
		
		// What does the stream look like when we try to print it?
		System.out.println(stream);
		
		// Java 8 println function over a stream
		stream.forEach(System.out::println);
	}

	private void unitTestingExample() {
		// Junit Example?
    	// Spock Example?    	
	}

	private void referenceObjects() {
		/** Part 1 */
		/* Create a vehicle object called minivan and print it */
        Vehicle minivan = new Vehicle("minivan", "Dodge Caravan RT");
        System.out.println("minivan before mixUpDasAuto: \t\t" + minivan);
        /* Call mixUpDasAuto, change the properties of the object and print */
        mixUpDasAuto(minivan);
        System.out.println("minivan after mixUpDasAuto: \t\t" + minivan);
        
        
        /* Set duplicate object to minivan and print the duplicate */
        Vehicle dupeMinivan = minivan;
        System.out.println("dupeMinivan before mixUpDasAuto2: \t" + dupeMinivan);
        
        /* Call mixUpDasAuto, change the properties of the object and print the */ 
        /* minivan and the duplicate. Are they the same? */ 
        mixUpDasAuto2(dupeMinivan);
        System.out.println("dupeMinivan after mixUpDasAuto2: \t" + dupeMinivan);
        System.out.println("minivan after mixUpDasAuto2: \t\t" +  minivan);
    }
    private void mixUpDasAuto(Vehicle auto) {
        auto.type = "car";
        auto.name = "Honda Civic";
        System.out.println("from inside mixUpDasAuto: \t\t" + auto);
    }
    private void mixUpDasAuto2(Vehicle auto) {
        auto.type = "suv";
        auto.name = "Honda Pilot";
        System.out.println("from inside mixUpDasAuto2: \t\t" + auto);
    }

    private void attemptSwap() {
        String initiallyA = "a";
        String initiallyB = "b";
        System.out.println("Pre-Op     => initiallyA is: " + initiallyA + " and initiallyB is: " + initiallyB);
        System.out.println(runAttemptSwap(initiallyA, initiallyB));
        System.out.println("Post-Op    => initiallyA is: " + initiallyA + " and initiallyB is: " + initiallyB);
    }

    private String runAttemptSwap(String initiallyA, String initiallyB) {
        String tempHolder = initiallyA;
        initiallyA = initiallyB;
        initiallyB = tempHolder;
        return "in method  => String initiallyA is now: " + initiallyA + " and string initiallyB is: " + initiallyB;
    }
    
    /**
     * The purpose of this example is to show how to calculate distinct
     * squares given a list of numbers.
     */
    private void numbersToArrayList() {
        List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);

        List<Integer> squaresList = numbers.stream()
                                           .distinct()
                                           .map(i -> i * i) // get list of unique squares
                                           .collect(Collectors.toList());
        System.out.println(squaresList);
        
        // Without Streams
        List<Integer> finalList = new ArrayList<Integer>();
        
        for (Integer num : numbers) {          // .stream
        	int square = num*num;              // .map
        	
			if (!finalList.contains(square)) { // .distinct
				finalList.add(square);         // .collect
			}
		}
        
        System.out.println(finalList);
    }

    /**
     * The purpose of this example is to show how to filter a list of names
     * and return a java.util.List as the result.
     * 
     * filter  - Returns a stream consisting of the elements of this stream 
     *           that match the given predicate.
     * map     - Returns a stream consisting of the results of applying the 
     *           given function to the elements of this stream.
     * collect - Performs a mutable reduction operation on the elements of this 
     *           stream. A mutable reduction is one in which the reduced value 
     *           is a mutable result container, such as an ArrayList, and elements 
     *           are incorporated by updating the state of the result rather than 
     *           by replacing the result.
     */
    private void stringsAndFilter() {
        List<String> nameList = Arrays.asList("John", "", "Johnson", "Johnny", "John boy", "", "Jack");
        List<String> filtered = nameList.stream()
                                        .filter(string -> !string.isEmpty())                          
                                        .map(String::toUpperCase)
                                        .filter(string -> string.contains("JO"))
                                        .collect(Collectors.toList()); // or .toSet() or .toMap() ...
        System.out.println("filtered names: " + filtered);
        
        // We can continue to pipeline the result
        int count = (int) filtered.stream()
        		                  .count();
        System.out.println("count: " + count);
    }

    /**
     * There are three main parts to a stream.
	 *  1. You start with a data source and invoke a stream method on it
     *  2. You can add one or more intermediate operations
     *  3. You end with a terminal operation
     */
    private void startRandomStream() {
    	// Let's:
    	//   1. invoke a stream by calling ints (part 1)
    	//   2. add an intermediate operation "limit" (part 2)
    	//   3. end with a terminal operation "foreach" (part 3)
        Random random = new Random();
        System.out.println("stand by for randomness");
        random.ints(5)                        // infinite data stream
              //.limit(5)                      // just pick 5; or pass a number into ints()
              .sorted()                      // sort the list
              .forEach(System.out::println); // print the list
    }

    /**
     * iterate - Returns an infinite sequential ordered Stream produced by iterative 
     * application of a function f to an initial element seed, producing a Stream 
     * consisting of seed, f(seed), f(f(seed)), etc. 
     * 
     * peek - Returns a stream consisting of the elements of this stream, additionally 
     *        performing the provided action on each element as elements are consumed 
     *        from the resulting stream. Peek is a non-interfering action to perform 
     *        on the elements as they are consumed from the stream.
     */
    private void startStreamWithIterate() {
        Stream.iterate(0, n -> n + 2)
              .peek(num -> System.out.println("Peeked at:" + num))
              .limit(5)
              .forEach(System.out::println);
    }

    public int divide(int a, int b) {
        int c = -1;
        
        try {
            c = a / b;
        } 
        catch (Exception e) {
            System.err.print("Exception ");
        } 
        finally {
            System.err.println("Finally ");
        }
        
        return c;
    }
    
    public static void main(String[] args) {
    	
    	int i = 32;
        float f = 45.0f; // widening primitive conversion
        double d = 45.0;
        Integer count = 0;
    	
    	
        new Week6Day3();
    	//File file = new File("dfjghl.txt");
    	//System.out.println("Done.");
    }
    
}
