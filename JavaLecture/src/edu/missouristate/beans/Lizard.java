package edu.missouristate.beans;

public class Lizard extends Animal {

	public Lizard() {
		// TODO Auto-generated constructor stub
	}
	
	public String soundX() {
		StringBuilder sb = new StringBuilder();
		
		for (int i=0; i < 2; i++) {
			sb.append("pffffssstt");	
		}
		
		return sb.toString();
	}
	
	public String soundX(String precursor) {
		return precursor + " " + sound();
	}

}
