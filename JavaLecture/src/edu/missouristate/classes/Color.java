package edu.missouristate.classes;
public enum Color {
    RED, BLUE, YELLOW, GREEN, ORANGE, PURPLE;
}
