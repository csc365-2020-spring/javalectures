package edu.missouristate.classes;

public class MyGenericClass2<T extends Integer> { 
    private T number;
    
    public void set(T object) { 
        this.number = object; 
    } 

    public T get() { 
        return number; 
    }
}