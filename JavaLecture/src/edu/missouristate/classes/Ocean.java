package edu.missouristate.classes;

public class Ocean extends MyAbstractWater { 

    @Override 
    public String getType() { 
        return Type.SALTY.getTypeLiteral(); 
    } 
}