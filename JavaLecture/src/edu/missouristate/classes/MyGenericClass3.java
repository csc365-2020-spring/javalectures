package edu.missouristate.classes;

import edu.missouristate.beans.Animal;

public class MyGenericClass3<T extends Animal> { 
    private T number;
    
    public void set(T object) { 
        this.number = object; 
    } 

    public T get() { 
        return number; 
    }
}