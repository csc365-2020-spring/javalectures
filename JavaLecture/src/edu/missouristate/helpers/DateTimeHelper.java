package edu.missouristate.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeHelper {

	public static String getTime(Long time, String format) {
		if (time == null || format == null) {
			return "";
		}
		
		Date date = new Date(time);
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(date);
	}

}
