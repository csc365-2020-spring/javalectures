package edu.missouristate.set1.exampleSet2.pojos;

public class Dog extends Animal {
    
    public Dog() {}

    public Dog(String type) {
        super(type);
    }

    public Dog(String type, String color) {
        super(type, color);
    }
    
    public String habitat() {
    	return "junk yard";
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Dog other = (Dog) obj;
        if (color == null) {
            if (other.color != null)
                return false;
        } else if (!color.equals(other.color))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Dog [type=" + type + ", color=" + color + "]";
    }
    
}
