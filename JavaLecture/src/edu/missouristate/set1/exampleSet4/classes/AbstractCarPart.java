package edu.missouristate.set1.exampleSet4.classes;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbstractCarPart extends AbstractProduct {
	private Timestamp manufacturedDate = new Timestamp(System.currentTimeMillis());
	private List<String> compatableCars = new ArrayList<String>();

	AbstractCarPart(String sku, String description) {
		super(sku, description);
	}

	abstract List<String> getStoreAvailabilty();

	public Timestamp getManufacturedDate() {
		return manufacturedDate;
	}

	public void setManufacturedDate(Timestamp manufacturedDate) {
		this.manufacturedDate = manufacturedDate;
	}

	public List<String> getCompatableCars() {
		if(compatableCars.size() == 0) {
			compatableCars.add("Sorry, no cars found");
		}
		return compatableCars;
	}

	public void setCompatableCars(List<String> compatableCars) {
		this.compatableCars = compatableCars;
	}
	
}
