package edu.missouristate.set1.exampleSet4.classes;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

public class Alternator extends AbstractCarPart {

	public Alternator(String sku, String description) {
		super(sku, description);
		// TODO Auto-generated constructor stub
	}

	@Override
	List<String> getStoreAvailabilty() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	BigDecimal getSalesPrice() {
		MathContext mc = new MathContext(2);
		return this.getMSRP().multiply(new BigDecimal(.65), mc);
	}

	@Override
	public String toString() {
		return "Alternator [getCompatableCars()=" + getCompatableCars() + ", getSalesPrice()=" + getSalesPrice()
				+ ", getManufacturedDate()=" + getManufacturedDate() + ", getSku()=" + getSku() + ", getDescription()="
				+ getDescription() + ", getMSRP()=" + getMSRP() + ", getClass()=" + getClass().getSimpleName() + "]";
	}

}
