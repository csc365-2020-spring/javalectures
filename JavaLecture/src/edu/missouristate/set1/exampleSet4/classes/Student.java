package edu.missouristate.set1.exampleSet4.classes;

public class Student {
    public int rollno;
    public String name;
    public int age;

    public Student(int rollno, String name, int age) {
        this.rollno = rollno;
        this.name = name;
        this.age = age;
    }
}