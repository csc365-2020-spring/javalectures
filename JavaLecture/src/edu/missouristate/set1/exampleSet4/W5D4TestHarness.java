package edu.missouristate.set1.exampleSet4;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import edu.missouristate.set1.exampleSet4.classes.AgeComparator;
import edu.missouristate.set1.exampleSet4.classes.Alternator;
import edu.missouristate.set1.exampleSet4.classes.NameComparator;
import edu.missouristate.set1.exampleSet4.classes.Player;
import edu.missouristate.set1.exampleSet4.classes.Student;
import edu.missouristate.set1.exampleSet4.interfaces.OrderImpl;
import edu.missouristate.set1.exampleSet4.pojos.Animal;
import edu.missouristate.set1.exampleSet4.pojos.Dolphin;
import edu.missouristate.set1.exampleSet4.pojos.Mammal;
import edu.missouristate.set1.exampleSet4.pojos.Reptile;

public class W5D4TestHarness {

	public W5D4TestHarness() {
//		testAnimalTime();
//		testConstructors(); // set debug to 1 in Animal and Mammal
//		testMammalForAnimal(); // set debug to 0 in Animal and Mammal
//		testSuperclassLists();
//		extendingExample(); // set debug to 1 in Animal and Mammal
//		testInterfaces();
//		abstractClass();
//		testLinkedList();
//		testComparable();
		testComparator();
	}

	private void abstractClass() {
		Alternator hondaAlt = new Alternator("alt01", "A fine alt for Honda Civics 2011-2015");
		hondaAlt.setMSRP(new BigDecimal("100.01"));
		System.out.println(hondaAlt);		
	}

	private void testSuperclassLists() {
		Reptile reptile = new Reptile();
		Mammal mammal = new Mammal();
		Animal animal = new Animal();

		List<Animal> animalList = new ArrayList<Animal>();
		animalList.add(reptile);
		animalList.add(mammal);
		animalList.add(animal);

		for (Animal currentAnimal : animalList) {
			System.out.println(currentAnimal.animalTime());
		}
	}

	private void testMammalForAnimal() {
		Mammal mammal = new Mammal();
		animalAsArgument(mammal);
	}

	private void animalAsArgument(Animal animal) {
		System.out.println(animal.animalTime());

//        if (animal instanceof Animal) {
//            System.out.println("Animal passed in is instance of Animal.");
//        }
//        
//        if (animal instanceof Mammal) {
//            System.out.println("Animal passed in is instance of Mammal.");
//        }
	}

	private void testConstructors() {
		Mammal mammal = new Mammal("furryDinos");
		System.out.println("mammal.getNumberOfSpecies():\t" + mammal.getNumberOfSpecies());
		System.out.println("mammal.getCuteName():\t\t" + mammal.getCuteName());
	}

	private void testAnimalTime() {
		Mammal mammal = new Mammal();
		System.out.println("mammal.animalTime():\t\t" + mammal.animalTime());
		System.out.println("mammal.invokeSuperTime():\t" + mammal.invokeSuperTime());
	}

private void testComparator() {
	List<Student> list = new ArrayList<Student>();
	list.add(new Student(101, "Robert", 27));
	list.add(new Student(106, "SuperClass", 21));
	list.add(new Student(105, "Ethan", 27));

	System.out.println("Sorting by Age and then By Name...");
	Collections.sort(list, new NameComparator());
	
	for (Student student : list) {
		System.out.println(student.rollno + " " + student.name 
				+ " " + student.age);
	}
	
	System.out.println("sorting by age only...");
	Collections.sort(list, new AgeComparator());

	for (Student student : list) {
		System.out.println(student.rollno + " " + 
				student.name + " " + student.age);
	}
}

	private void testComparable() {
		Player player1 = new Player("Bob", 2);
		Player player2 = new Player("Sue", 1);
		LinkedList<Player> playerList = new LinkedList<Player>();
		playerList.add(player1);
		playerList.add(player2);
		Collections.sort(playerList);
		
		// comparing objects with .compareTo() is not the same thing
//		System.out.println("p1.compareTo(p2): " + player1.compareTo(player2));
		
		
		/** Example 1 */
		// Edit the compare function in Player
		// Ensure sort by rank is uncommented
		
		/** Example 2 */
		// Edit the compare function in Player
		// Ensure sort by name is uncommented
		
		for (Player player : playerList) {
			System.out.println(player);
		}
	}

	private void testLinkedList() {
		// Creating object of class linked list
		LinkedList<String> linkedList = new LinkedList<String>();

		// Adding elements to the linked list
		linkedList.add("A");      // queue
		linkedList.add("B");      // queue
		linkedList.addLast("C");  // add last!
		linkedList.addFirst("D"); // add first!
		linkedList.add(2, "E");   // insert into position [2]
		linkedList.add("F");      // queue
		linkedList.add("B");      // queue
		linkedList.add("G");      // queue 
//		System.out.println("Linked list : " + linkedList); // output?
		
		
		// stop 
		// stop
		// letters : D A E B C F B G 
		// position: 0 1 2 3 4 5 6 7 
		
		// Removing elements from the linked list
		linkedList.remove("B");
		linkedList.remove(3);
		linkedList.removeFirst();
		linkedList.removeLast();
//		System.out.println("Linked list after deletion: " + linkedList); // ?

		
		
		// stop
		// stop
		// letters : A E F B
		// position: 0 1 2 3 4 5 6 7
		
		
		
		// Finding elements in the linked list
		boolean status = linkedList.contains("E");

//		if (status)
//			System.out.println("List contains the element 'E' ");
//		else
//			System.out.println("List doesn't contain the element 'E'");

		// Number of elements in the linked list
		int size = linkedList.size();
		System.out.println("Size of linked list = " + size);

		// A E F B
		// Get and set elements from linked list
		Object element = linkedList.get(2);
		System.out.println("Element returned by get() : " + element);
		linkedList.set(2, "Y");
		System.out.println("Linked list after change : " + linkedList);
	}



	private void testInterfaces() {
		OrderImpl oi = new OrderImpl();
		oi.showNumberOfItems(3);
		oi.showOrderType();
		oi.showDate(); // default method
		oi.showStoreNumber(2345);
		oi.showSpecialOrderId(322);
		
		oi.showDate(); // uncomment out RetailOrder's override
		oi.showDate("Pat"); // overloading
	}

	private void extendingExample() {
		Dolphin d = new Dolphin();
		System.out.println("d.animalTime():\t\t" + d.animalTime());
		System.out.println("d.invokeSuperTime():\t" + d.invokeSuperTime());
		System.out.println("ANIMAL_COUNTER:" + Animal.ANIMAL_COUNTER);
	}

//    private void testConstructors() {
//        Mammal mammal = new Mammal("intern");
//        System.out.println("mammal.getI():\t" + mammal.getI());
//        System.out.println("mammal.getJ():\t" + mammal.getJ());
//    }

//    private void testMammalForAnimal() {
//        Mammal mammal = new Mammal();
//        animalAsArgument(mammal);
//    }
//    private void animalAsArgument(Animal animal) {
//        System.out.println(animal.animalTime());
//    }

//    private void animalAsArgument(Animal animal) {
//
//        if (animal instanceof Animal) {
//            System.out.println("Animal passed in is instance of Animal.");
//        }
//
//        if (animal instanceof Mammal) {
//            System.out.println("Animal passed in is instance of Mammal.");
//        }
//    }

	public static void main(String[] args) {
		new W5D4TestHarness();
	}
}
