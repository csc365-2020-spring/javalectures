package edu.missouristate.set1.exampleSet4.interfaces;

import java.util.Date;

public interface GenericOrderInterface {
	public void showNumberOfItems(int numberOfItems);
	public void showOrderType();
	
	default void showDate() {
		System.out.println(new Date());
	}

}
