package edu.missouristate.set1.exampleSet4.interfaces;

import java.util.Date;

public class OrderImpl implements RetailOrderInterface {

	@Override
	public void showNumberOfItems(int numberOfItems) {
		System.out.println("Number of items in order: " + numberOfItems);		
	}

	@Override
	public void showStoreNumber(int storeNumber) {
		System.out.println("Order placed at store: " + storeNumber);
	}

	@Override
	public void showOrderType() {
		System.out.println("Order type: " + OrderImpl.ORDER_TYPE);		
	}

	@Override
	public void showSpecialOrderId(int id) {
		System.out.println("Special order id:" + id);		
	}
	
	// Overloading showDate()
	public void showDate(String name) {
		  System.out.println("Hello, " + name + " today's date is " + new Date());
		}


}
