package edu.missouristate.set1.exampleSet4.pojos;

public class Animal {
	public static int ANIMAL_COUNTER = 0;
	public int numberOfSpecies;
	public String cuteName;
	int debug = 1;
	public final String DESCRIPTION = "Animal"; 
	
	public Animal() {
		++ANIMAL_COUNTER;
		if (debug > 0) System.out.println("Animal Empty Constructor called!");
	}
	
	public Animal(int numberOfSpecies, String cuteName) {
		this.numberOfSpecies = numberOfSpecies;
		this.cuteName = cuteName;
		if (debug > 0) System.out.println("Animal Constructor(int, String) "
				+ "called! numberOfSpecies="+numberOfSpecies+" and cuteName="+cuteName);
	}

	public String animalTime() {
		return "It's animal time!";
	}
	
	// getters and setters
	public int getNumberOfSpecies() {
		return numberOfSpecies;
	}

	public void setNumberOfSpecies(int numberOfSpecies) {
		this.numberOfSpecies = numberOfSpecies;
	}

	public String getCuteName() {
		return cuteName;
	}

	public void setCuteName(String cuteName) {
		this.cuteName = cuteName;
	}

}
