package edu.missouristate.set1.exampleSet4.pojos;

public class Mammal extends Animal {
	int debug = 1;
	public final String DESCRIPTION = "Mammal";
	
	public Mammal() {
		if (debug > 0) System.out.println("Mammal constructor called!");
	}

	public Mammal(String cuteName) {
		super(5450, cuteName);
		if (debug > 0) System.out.println("Mammal constructor called");
	}

	public String animalTime() {
		return "It's mammal time!";
	}

	public String invokeSuperTime() {
		System.out.println(super.DESCRIPTION);
		System.out.println(DESCRIPTION);
		return super.animalTime();
	}
}
