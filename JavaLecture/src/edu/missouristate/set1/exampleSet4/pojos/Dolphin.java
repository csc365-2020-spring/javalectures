package edu.missouristate.set1.exampleSet4.pojos;

public class Dolphin extends Mammal {
	int debug = 1;
    private String myString;
    
    public Dolphin() {
    	// Constructor call must be the first statement in a constructor
//    	this("Dolphin empty constuctor calling another dolphin constructor...");
    	if (debug > 0 ) System.out.println("Dolphin empty constructor called.");
    }
    
    Dolphin(String str){
        myString = str;
        if (debug > 0 ) System.out.println("Dolphin Message from first constructor: " + myString);
    }
    
    public String animalTime() {
        return "It's dolphin time!";
    }

    public void printConstructorMessage() {
    	System.out.println(myString);
    }
    
    // Not required since the superclass Mammal has invokeSuperTime()
//    public String invokeSuperTime() {
//        return super.animalTime();
//    }
}
