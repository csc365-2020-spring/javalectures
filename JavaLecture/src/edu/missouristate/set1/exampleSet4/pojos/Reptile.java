package edu.missouristate.set1.exampleSet4.pojos;

public class Reptile extends Animal {
	
	public String animalTime() {
		return "It's reptile time!";
	}

	public String invokeSuperTime() {
		return super.animalTime();
	}
}