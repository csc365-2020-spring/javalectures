package edu.missouristate.set1.exampleSet1;

public class SmartClock extends SmartProtectedClock {
    
    public long getTimeInSeconds() {
    	System.out.println("from SmartClock: this.getClass():" + this.getClass());
        return this.time / 1000;
    }
}
