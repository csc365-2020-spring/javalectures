package edu.missouristate.set1.exampleSet1;

import java.util.Date;

public class SmartProtectedClock {
	
	protected long time = new Date().getTime();
	
	public SmartProtectedClock() {
		// We can access the protected variables within 
		// the same class
		System.out.println("from Smart Protected Clock constructor: " + time);
	}

    public static void main(String[] args) {
    	new SmartProtectedClock();
    }
    
}

