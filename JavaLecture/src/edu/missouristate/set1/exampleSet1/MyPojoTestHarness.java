package edu.missouristate.set1.exampleSet1;

import edu.missouristate.set1.exampleSet1.pojos.MyPojo;

public class MyPojoTestHarness {

	public MyPojoTestHarness() {
		System.out.println(MyPojo.accessible);
		MyPojo mp = new MyPojo();
		System.out.println(mp.getCount());
		mp.incrementCount();
		System.out.println(mp.getCount());
		
		System.out.println(mp.getEncapsulated().toString());
		
		if (mp.getEncapsulated() == "" || mp.getEncapsulated() == null) {
			System.out.println("Dude, seriously!");
		}
		
		mp.setEncapsulated("uh uhh yeah");
		System.out.println(mp.getEncapsulated());
		
		
		
	}

	public static void main(String[] args) {
		new MyPojoTestHarness();
	}

}
