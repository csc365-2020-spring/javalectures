package edu.missouristate.set1.exampleSet1.pojos;

import java.math.BigDecimal;

public class MyPojo {
	
	public final BigDecimal PI;// = new BigDecimal("3.14"); 
	public static String accessible = "Pfhhhhh";
	private String encapsulated;
	private int count = 0;
	
	public MyPojo() {
		PI = new BigDecimal("3.14");
	}

	public void incrementCount() {
		count += 1;	
	}
	
	public static String getAccessible() {
		return accessible;
	}

	public static void setAccessible(String accessible) {
		MyPojo.accessible = accessible;
	}

	public String getEncapsulated() {
		return encapsulated;
	}

	public void setEncapsulated(String encapsulated) {
		this.encapsulated = encapsulated;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public BigDecimal getPI() {
		return PI;
	}

	
	
}
