package edu.missouristate.set1.exampleSet1.classes;

import edu.missouristate.classes.DefaultClockTimex;
import edu.missouristate.set1.exampleSet1.SmartClock;

public class WorldClass {

    public WorldClass() {
    	/**
    	 * TEST PROTECTED ACCESS FROM ANOTHER CLASS
    	 */
        // Can I access a protected resource from this "World Class" 
        // Try to access the protected variable "time" in the protected 
    	// class "Protected Clock" using an instance of SmartClock
        
    	SmartClock clock = new SmartClock();
        //System.out.println(clock.time);
        
        // ^== ERROR: The field ProtectedClock.time is not visible
        // We cannot access this protected member from "World" 
    	// Classes (or classes outside the package)
    }

    public static void main(String[] args) {
        new WorldClass();
    }

}
