package edu.missouristate.set1.exampleSet1.classes;


public class Kitten extends Cat {

    private Integer fluffyLevel;
    private static final long serialVersionUID = -2910862051266573394L;
    
    public Kitten() {
        super();
    }

    public Kitten(String name, String breed) {
        super(name, breed);
    }

    public Integer getFluffyLevel() {
        return fluffyLevel;
    }

    public void setFluffyLevel(Integer fluffyLevel) {
        this.fluffyLevel = fluffyLevel;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((fluffyLevel == null) ? 0 : fluffyLevel.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Kitten other = (Kitten) obj;
        if (fluffyLevel == null) {
            if (other.fluffyLevel != null)
                return false;
        } else if (!fluffyLevel.equals(other.fluffyLevel))
            return false;
        return true;
    }

    @Override
    public String toString() {               
        return "Kitten [name=" + getName() + ", breed=" + getBreed() + ", fluffy level=" + fluffyLevel + "]";
    }
    
}
