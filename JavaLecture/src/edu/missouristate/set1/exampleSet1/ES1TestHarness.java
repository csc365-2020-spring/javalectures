package edu.missouristate.set1.exampleSet1;

import java.util.Date;

import edu.missouristate.helpers.DateTimeHelper;
import edu.missouristate.set1.exampleSet1.classes.DefaultClockTimex;

public class ES1TestHarness {

    public ES1TestHarness() {
    	/* PUBLIC */
        // Test the clock with a public access modifier
    	// For the fun of it, we will convert the Time for this 
    	// example. Going forward we will simply return a "Long."
//        ClockReader clockReader = new ClockReader();
//        Long time = clockReader.readClock();
//        String timeStr = DateTimeHelper.getTime(time, "HH:mm:ss.SSS"); 
//        System.out.println("time: " + time + " timeStr: " + timeStr);
        
        // FYI - Template Literal "Like" Syntax
        // https://docs.oracle.com/javase/tutorial/java/data/numberformat.html
        //System.out.println(String.format(".format time: %d timeStr: %s", time, timeStr));
        
        /* PROTECTED */
        // Take 1 (Subclass)
    	// Visit SmartProtectedClock.java: change "public long time" 
    	// to "protected long time"
        // Test the clock with a protected access modifier
        // View WorldClass.java for this example (it is in another package)
    	//   Another Package = "World Access"
    	//  Uncomment the following line in WorldClass.java
    	//    System.out.println(clock.time);
    	// The code will not compile since the member is protected 
    	
    	// Take 2
    	// If we extend another class (subclass), we have access to the
    	// protected super class members
    	// View SmartClock.java
    	// When "this" is printed, does it refer to SmartClock or
    	// SmartProtectedClock?
//        SmartClock mySmartClock = new SmartClock();
//        System.out.println("mySmartClock.time: " + mySmartClock.time);
//        System.out.println("mySmartClock.getTimeInSeconds(): " + mySmartClock.getTimeInSeconds());
        
        /* DEFAULT */
        // Test the Default Clock 
    	// Check out DefaultClock.java
    	//    long time = new Date().getTime(); 
    	// This member variable does not have an access modifier (default)
        ///DefaultClockTimex timex = new DefaultClockTimex();
        ///System.out.println("timex.time:" + timex.time); 
        // ^== ERROR: The field DefaultClock.time is not visible
        
        /* PRIVATE */
        // Visit Clock.java: change "public long time" to 
    	//    "private long time"
        // We can only access "time" from within the class with 
    	//   this modifier
    	// ClockReader.java will not compile!
    }

    public static void main(String[] args) {
        new ES1TestHarness();
    }

}
