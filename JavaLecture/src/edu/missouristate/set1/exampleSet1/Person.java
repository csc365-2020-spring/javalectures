package edu.missouristate.set1.exampleSet1;

public class Person {
    
    private String name;
    private int id;
    
    public Person(String name, int id) {
        this.name = name;
        this.id = id;        
    }

    public void printPerson() {
        System.out.println("Name: " + name + "\nid: " + id);
    }
        
}
