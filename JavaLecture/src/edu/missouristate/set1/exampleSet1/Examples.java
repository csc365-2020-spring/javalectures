package edu.missouristate.set1.exampleSet1;

import java.util.ArrayList;
import java.util.List;

public class Examples {

	public static void main(String[] args) {
		
		/* Hello World example
		 * note about Eclipse auto complete with sysout
		*/ 
		System.out.println("Hello World!");
		
		// byte 
		byte a=68;
		
		// short
		short x=3, y=6;
		
		// int
		int z = 1;
		
		// long
		long l = 45L, m=30l;
		
		// float
		float f = 45F, fl=30f;
		
		// double
		double d = 3.0, d2=2.0d, d3=3.0D;
		
		// boolean
		boolean truthy = true;
		
		// char
		// single quotes
		char c = 'c';
		
		/* Autoboxing
		 * auto converting a primitive into an object
		 * create a list that holds Integer objects
		 * add primitive ints to the list
		 * */
		List<Integer> arrayList = new ArrayList<Integer>();
		for (int i = 1; i < 10; i += 2) {
		    arrayList.add(i);
		}
		//System.out.println("arrayList:" + arrayList);
		
		/*Unboxing
		 * converting an object to it's corresponding primitive		
		*/
		int sum = 0;
		for (Integer i : arrayList) {
            sum += 1; 
        }
		//System.out.println("sum:" + sum);
		
	}
}
