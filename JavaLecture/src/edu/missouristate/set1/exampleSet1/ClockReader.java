package edu.missouristate.set1.exampleSet1;

public class ClockReader {
    
    Clock clock = new Clock();

    public long readClock(){
        return clock.time;
    }

    public Clock getClock() {
        return clock;
    }
    
}
