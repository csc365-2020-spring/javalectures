package edu.missouristate.set1.exampleSet1;

import java.math.BigDecimal;

public class BigDecimalTestHarness {

	public BigDecimalTestHarness() {
		BigDecimal one = new BigDecimal("1");
		BigDecimal two = new BigDecimal("2");
		System.out.println("one.add(two): " + one.add(two));
		System.out.println("one: " + one);
		
		one = one.add(two);
		System.out.println("one: " + one);
	}

	public static void main(String[] args) {
		new BigDecimalTestHarness();
	}

}
