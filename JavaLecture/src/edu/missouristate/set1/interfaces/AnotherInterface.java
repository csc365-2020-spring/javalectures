package edu.missouristate.set1.interfaces;

public interface AnotherInterface {
    public void doStuff();
    public void dontDoStuff();
    
    //@Override
    default String doSomethingElse(String x, String y) {
        return "x:" + x + " and y: " + y;
    }
    
}
