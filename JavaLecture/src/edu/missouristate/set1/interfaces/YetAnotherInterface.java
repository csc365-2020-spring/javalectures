package edu.missouristate.set1.interfaces;

public interface YetAnotherInterface {
    public void win();
    public void winHarder();
}
