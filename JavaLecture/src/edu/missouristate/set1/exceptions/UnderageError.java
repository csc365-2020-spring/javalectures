package edu.missouristate.set1.exceptions;
import org.apache.log4j.spi.ErrorCode;

public class UnderageError extends Exception {

    private ErrorCode code;
    private static final long serialVersionUID = 893577274883254367L;

    public UnderageError(String message) {
        super(message, new Throwable(message));
    }
    
    public UnderageError(String message, Throwable cause) {
        super(message, cause);
    }
    
    public UnderageError(String message, Throwable cause, ErrorCode code) {
        super(message, cause);
        this.code = code;
    }
    
    public ErrorCode getErrorCode() {
        return code;
    }
}