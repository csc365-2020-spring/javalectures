package edu.missouristate.set1.exampleSet3.classes;

import edu.missouristate.set1.exampleSet3.pojos.Animal;

public class MyGenericClass3<T extends Animal> { 
    private T number;
    
    public void set(T object) { 
        this.number = object; 
    } 

    public T get() { 
        return number; 
    }
}