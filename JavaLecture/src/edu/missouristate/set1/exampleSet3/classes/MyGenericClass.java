package edu.missouristate.set1.exampleSet3.classes;

public class MyGenericClass<T> {
    private T object; 
    
    public void set(T object) { 
        this.object = object; 
    } 

    public T get() { 
        return object; 
    }
}