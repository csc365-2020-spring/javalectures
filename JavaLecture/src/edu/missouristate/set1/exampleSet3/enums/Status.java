package edu.missouristate.set1.exampleSet3.enums;

import org.apache.commons.text.WordUtils;

public enum Status {
	ACTIVE(1), 
	INACTIVE(2), 
	WAITING_CONFIRMATION(3);

	private final int statusCode;

	Status(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getStatusInt(Status status) {
		return statusCode;
	}

	public String getStatusLiteral() {
		return this.name();
	}

	@Override
	public String toString() {
		return " ::" + this.name();
	}
	
	public String getViewableStatusLiteral() {
		String viewableStatus = getStatusLiteral();

		if (viewableStatus.indexOf("_") > 0) {
			String finalViewableStatus = viewableStatus.replaceAll("_", " ");
			finalViewableStatus = WordUtils.capitalizeFully(finalViewableStatus);
			return finalViewableStatus;
		} else {
			return viewableStatus;
		}
	}
}
