package edu.missouristate.set1.exampleSet3.enums;

import org.apache.commons.text.WordUtils;

public enum Month {
	// Each Enum value is "public static final" (implied) 
	JANUARY("Q1", "WINTER"),
    FEBRUARY("Q1", "WINTER"),
    MARCH("Q1", "SPRING"),
    APRIL("Q2", "SPRING"),
    MAY("Q2", "SPRING"),
    JUNE("Q2", "SUMMER"),
    JULY("Q3", "SUMMER"),
    AUGUST("Q3", "SUMMER"),
    SEPTEMBER("Q3", "FALL"),
    OCTOBER("Q4", "FALL"),
    NOVEMBER("Q4", "FALL"),
    DECEMBER("Q4", "WINTER");
	
    private final String quarter;
    private final String season;
    
    Month(String quarter, String season) {
        this.quarter = quarter;
        this.season  = season;
    }
    
	public String getQuarterAndSeason() {
    	return this.quarter + " " + WordUtils.capitalizeFully(this.season);
    }
    
    public String getQuarter() {
    	return quarter;
    }
    
    public String getSeason() {
    	return season;
    }
}
